import requests
import xml.etree.cElementTree as ET
import xml.dom.minidom as minidom
import datetime


def get_stations() -> list:
    url = 'https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_wr/en/station_information.json'
    resp = requests.get(url=url)
    data = resp.json()  # Check the JSON Response Content documentation below
    return data["data"]["stations"]


def prettify(elem) -> str:
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    parsed = minidom.parseString(rough_string)
    return parsed.toprettyxml(indent="\t")


if __name__ == '__main__':
    gpx = ET.Element("gpx",
                     {"version": "1.1",
                      "creator": "OsmAnd~ 4.3.12",
                      "xmln": "http://www.topografix.com/GPX/1/1",
                      "xmlns:osmand": "https://osmand.net",
                      "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
                      "xsi:schemaLocation": "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"}
                     )
    metadata = ET.SubElement(gpx, "metadata")

    ET.SubElement(metadata, "name").text = "NextBike"
    ET.SubElement(metadata, "time").text = datetime.datetime.now().astimezone().isoformat()

    for station in get_stations():
        wpt = ET.SubElement(gpx, "wpt", {"lat": str(station["lat"]),
                                                   "lon": str(station["lon"])}
                            )
        name = ET.SubElement(wpt, "name").text = station["name"]
        type = ET.SubElement(wpt, "type").text = "NextBike"
        desc = ET.SubElement(wpt, "desc").text = station["rental_uris"]["android"]

        extensions = ET.SubElement(wpt, "extensions")
        osmand_icon = ET.SubElement(extensions, "osmand:icon").text = "bicycle_transport"
        osmand_background = ET.SubElement(extensions, "osmand:background").text = "circle"
        osmand_color = ET.SubElement(extensions, "osmand:color").text = "#d00d0d"

    extensions = ET.SubElement(gpx, "extensions")
    osmand_points_groups = ET.SubElement(extensions, "osmand:points_groups")
    group = ET.SubElement(osmand_points_groups, "group", {"name": "NextBike",
                                                                    "color": "#d00d0d",
                                                                    "icon": "bicycle_transport",
                                                                    "background": "circle"}
                          )

    pretty_xml = prettify(gpx)

    print(pretty_xml)

    with open("NextBikes.gpx", "w") as file:
        file.write(pretty_xml)

