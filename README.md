# NextBike 4 OsmAnd~

Take data from [here](https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_wr/en/station_information.json) about [NextBike](https://www.nextbike.net/en/) and create `.gpx` file to import it into [OsmAnd~](https://f-droid.org/packages/net.osmand.plus/).

![](screenshot.png)